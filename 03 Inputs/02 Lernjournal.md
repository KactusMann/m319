# Input: Lernjournal

Die Inhalte dieses Inputs beziehen sich grundsätzlich auf die Ausführungen im Dokument [/00 Organisatorisches/Inputs.md](../00%20Organisatorisches/Inputs.md) dieses Repositories.

## Form

Das Lernjournal ist ein schriftlicher Eintrag, welcher durch alle Lernenden jeweils am Ende eines Halbtages im Abschnitt *Lernjournal M319* des *Klassennotizbuches* gemacht wird. Das Klassennotizbuch ist via *Teams* im Kanal *Allgemein* über die Registerkarte *Klassennotizbuch* erreichbar. **Alternativ kann das Klassennotizbuch auch über die Applikation OneNote geöffnet und synchronisiert werden.**

Zum *Umfang* eines Eintrages existieren keine allgemeinen Vorgaben. Die Lernenden sollten mit jedem Eintrag im Lernjournal sicherstellen, dass alle wesentlichen Inhalte (siehe unten) eines Halbtages in angemessener Tiefe festgehalten werden.

## Inhalte

Das Lernjournal kann als persönliches Lerntagebuch betrachtet werden. Darin werden alle *persönlich Relevanten* Aspekte des Lernens rund um das Modul 319 festgehalten. Dazu ghört beispielsweise eine kurze Zusammenfassung der Tätigkeiten und bearbeiteten Inhalte. Mindestens ebenso wichtig sind dabei jedoch persönliche Gedanken und Gefühle rund um den Arbeitsprozess. Auch die Planung und Auswertung des individuellen Lernfortschritts sollen im Lernjournal einfliessen.

### Mögliche Fragestellungen

Nachfolgend sind einige Fragestellungen aufgeführt, welche in einem Eintrag des Lernjournals thematisiert werden könnten. Es ist dabei wichtig, nicht einzelne Fragen zu beantworten, sondern einen zusammenhängenden Text zu verfassen. Dabei ist es durchaus sinnvoll, verschiedene Schwerpunkte zu setzen. Es ist also kaum angebracht, *alle* der nachfolgenden Fragestellungen in *jedem* Eintrag im Lernjournal gleichermassen zu berücksichtigen.

- Welche Kompetenzen habe ich heute bearbeitet?
- Die Erreichtung welcher Kompetenzen konnte ich heute nachweisen?
- Wie habe ich mich beim Kompetenznachweis gefühlt (stolz, erleichtert, enttäuscht, ernüchtert, ...)?
- Welche Form(en) des Kompetenznachweis habe ich gewählt?
- Welche Form(en) des Kompetenznachweis mag ich? Was finde ich eher schwierig und/oder mühsam?
- Welche Ziele habe ich heute erreicht (inhaltlich, prozessorientiert, ...)?
- Welches sind meine primären Ziele für den nächsten Halbtag (inhaltlich, prozessorientiert, ...)?
- Welche Faktoren waren für meinen Arbeits- und Lernfortschritt hilfreich? Was war eher hinderlich? Wieso?
- Welche Arbeitsformen und -materialien habe ich eingesetzt (Einzelarbeit vs. Partnerarbeit; Texte, Tutorials, Videos, Programmbeispiele, ...)?
- Welche Vorkenntnisse hatte ich bereits (Sek, private Erfahrungen, Betrieb, ...) und inwiefern konnte ich daran anknüpfen?
- Wie schätze ich meine Leistung, meinen Arbeitsfortschritt, meine Kompetenzen im Vergleich mit anderen Lernenden ein? Wie fühle ich mich dabei?
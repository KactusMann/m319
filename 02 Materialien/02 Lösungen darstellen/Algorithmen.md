# Algorithmen

Für die Erstellung von Struktogrammen eignen sich verschiedene Algorithmen. Die nachfolgenden Algorithmen wurden so ausgewählt, dass in den meisten Beispielen jeweils mindestens eine Variante *Selektion* und eine Variante der *Iteration* zur Darstellung notwendig sind.

## Probleme mit vorgegebenem Algorithmus

Für die nachfolgenden Probleme ist der jeweilige Algorithmus in abstrakter, textueller Form vorgegeben.

### Wurzelziehen nach Archimedes

Archimedes (um 287-212 v. Chr.) hat für die Berechnung der Quadratwurzel einer Zahl n folgende Formel angegeben:

```math
x_{neu} = \frac{1}{2}(x_{alt} + \frac{n}{x_{alt}})
```

Der Algorithmus zur Verwendung dieser Formel lautet:

- Zahl `n` einlesen
- `xneu`= `n`
- Wiederhole:
  - `xalt` = `xneu`
  - `xneu`= (`xalt` + `n`/`xalt`)/2
- solange | `xneu` - `xalt` | > epsilon

Dabei steht epsilon für die *Genauigkeit*. Sinnvolle Werte für epsilon sind beispielsweise $`10^{-4}`$ (0.0001).

### Berechnen des grössten gemeinsamen Teilers (GGT)

Um den GGT zweier Zahlen `a` und `b` zu berechnen, kann dieser Algorithmus verwendet werden:

- `a` und `b` einlesen
- Wiederhole:
  - Berechne den Rest `r` der Ganzzahldivision `a` / `b`
  - Ersetze `a` durch `b`
  - Ersetze `b` durch den berechneten Rest
- solange der Rest `r` ungleich `0` ist.
- Der letzte Wert von `a` ist der gesuchte GGT.

### Fibonaccizahlen berechnen

Die Fibonacci-Zahlen sind eine aufsteigende Zahlenreihe. Jedes Element entpricht der Summe seiner beiden Vorhängerelemente: 1, 1, 2, 3, 5, 8, 13, 21, ...

Der nachfolgende Algorithmus kann verwendet werden, um alle Fibonaccizahlen, welche kleiner als ein definierter Schwellenwert `max` sind, auszugeben:

- `max` einlesen
- `a` den Wert `1` zuweisen
- `b` den Wert `1` zuweisen
- Wiederhole
  - `a` ausgeben
  - `c` = `a` + `b`
  - `a`= `b`
  - `b`= `c`
- solange `a` < `max`

## Probleme ohne vorgegebenem Algorithmus

Im Verzeichnis [99 Mögliche Aufgabenstellungen](../99%20Mögliche%20Aufgabenstellungen/) existieren zahlreiche Probleme, für welche ein Algorithmus formuliert werden kann. Diese Algorithmen können anschliessend als Struktogramm dargestellt werden.
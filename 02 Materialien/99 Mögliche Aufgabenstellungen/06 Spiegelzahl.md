# Spiegelzahl

Erstellen Sie ein Programm, welches die einzelnen Ziffern einer mehrstelligen Zahl spiegelt. Die Zahl soll in "umgekehrter Reihenfolge" ausgegeben werden.

> Beachten Sie, dass Zahlen, welche ein Vielfaches von `10` sind, kein "numerisches Spiegelbild" haben.
> *Grund:* Führende Nullen werden üblicherweise nicht geschrieben.
> *Beispiel:* `140` würde gespiegelt die Zahl `041` ergeben, welche jedoch im numerischer Schreibweise als `41` geschrieben wird.

## Beispiele

- `12345` → `54321`
- `101` → `101`
- `18321` → `12381`

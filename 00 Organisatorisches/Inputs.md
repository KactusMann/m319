# Inputs

Der Erwerb von Wissen und Kompetenzen durch die Lernenden erfolgt im Modul 319 grundsätzlich selbstgesteuert und individuell. In regelmässigen Abständen findet jedoch sogenannte *Inputs* statt. Im Rahmen dieser Inputs greift die Lehrperson ein aktuelles, relevantes Thema im Kontext der Unterrichtsinhalte auf, präsentiert dieses, illustiert anhand von Beispielen und geht auf Fragen der Lernenden ein. Die Inhalte dieser Inputs sind teilweise im Voraus geplant. Andere Inputs greifen aktuelle Fragen und Probleme auf, welche im Verlauf des individuellen Lernprozess' mehrfach aufgetreten sind.

## Zeitplan

Die Inputs finden jeweils zu klar definierten Zeitpunkten während dem regulären Unterricht statt. Grundsätzlich findet ein Input pro Unterrichtshalbtag statt, wobei zu Beginn und gegen Ende des Moduls vermehrt Abweichungen von dieser Regel möglich sind.

Ein Input dauert jeweils rund 30 Minuten. Die genaue Dauer hängt vom Thema (Umfang, Komplexität) sowie auch vom Interesse der anwesenden Lernenden ab. Bei regem Austausch (erwünscht!), interessanten Fragen (erwünscht!) und vielen Querverweisen sowie Exkursen (möglich!) kann die Dauer eines Input auch gegen eine Stunde tendieren.

| Halbtag | Zeit  | Thema |
| :-----: | :--:  | :---- |
| 1       | 11:00 | **Einrichtung der persönlichen Programmierumgebung**: *Visual Studio Code* und die Erstellung von Programmierprojekten mit *C#* in *.NET 5.0* |
| 2       | 14:45 | **Lernjournal**: Form, Inhalte, Nutzen, Beispiele |
| 3       | 10:00 | **Input und Output**: Daten von der Konsole einlesen und auf die Konsole ausgeben |
| 4       | 14:10 | **Operatoren**: Übersicht und Anwendungsbeispiele |
| 5       | 10:00 | **Struktogramm - Praktisches Beispiel**: Gemeinsames Erstellen eines Struktogramm für die Aufgabe [06 Spiegelzahl](../02%20Materialien/99%20Mögliche%20Aufgabenstellungen/06%20Spiegelzahl.md) |
| 6       | 14:10 | **Live Coding**: Implementation einer möglichen Lösung für die Aufgabe [06 Spiegelzahl](../02%20Materialien/99%20Mögliche%20Aufgabenstellungen/06%20Spiegelzahl.md) anhand des zuvor erstellten Struktogramms |

Die Termine und Inhalte weiterer Inputs werden an dieser Stelle fortlaufend ergänzt...

## Teilnahme an den Inputs

Die Teilnahme an den Inputs ist grundsätzlich freiwillig. Es steht allen Lernenden frei, ob und an welchen Inputs sie teilnehmen möchten. Die Teilnahme ist selektiv, je nach Thema möglich. Damit die Inputs für alle Beteiligten möglichst interessant und attraktiv und sind, sollen die nachfolgenden Regeln von *allen* eingehalten werden.

### Wer anwesend ist nimmt teil

Die Inputs finden im Klassenzimmer statt. Wer während der Dauer eines Inputs im Klassenzimmer anwesend ist, nimmt am Input teil. Die individuellen Arbeiten werden während des gesamten Inputs unterbrochen.

Wer nicht an einem Input teilnehmen möchte, verlässt für die Dauer des Inputs das Klassenzimmer.

### Inputs sind Unterrichtszeit

Die Inputs finden während der regulären Unterrichtszeit statt. Die Teilnahme ist für alle Lernenden möglich jedoch nicht zwingend. Wer *nicht* an einem Input teilnehmen möchte, arbeitet für die gesamte Dauer des Inputs selbständig an den Inhalten des Modul 319.

Die Freiwilligkeit zur Teilnahme an einem Input kann nicht mit *optionaler Arbeitszeit* gleichgesetzt werden.

### Zeiten werden eingehalten

Während der Unterrichtszeit im Modul 319 arbeiten alle Lernenden individuell an den verschiedenen Kompetenzen. Die Inhalte werden selbständig ausgewählt und der Arbeitsfortschritt von allen Lernenden selber geplant, überwacht und reflektiert. Verbindliche Arbeitszeiten sind eine wesentliche Grundlage für das Funktionieren dieses Lern- und Arbeitsprozess im Kollektiv.

Die Inputs beginnen daher genau zur geplanten Zeit. Über allfällige Abweichungen informiert die Lehrperson frühzeitig. Wer an einem Input teilnehmen möchte, ist spätestens 5 Minuten vor Beginn im Klassenzimmer. Wer *nicht* an einem Input teilnehmen möchte, verlässt das Klassenzimmer spätestens 5 Minuten vor Beginn des Inputs und setzt die Arbeit an einem anderen Ort fort.

### Alles oder Nichts

Bei den Inputs handelt es sich um didaktisierte Unterrichtssequenzen der Lehrperson. Um die Inputs bzw. die teilnehmenden und präsentierenden Personen nicht zu stören, ist eine verspätete Teilnahme oder das frühzeitige Verlassen eines Inputs nicht erwünscht.